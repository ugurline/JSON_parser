package json.json.read;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Application {

	public static void main(String[] args) throws JSONException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		int numberOfCities=2000;

        try {
        	
        	String path = System.getProperty("user.dir");
        	String inputFileName = "/city.list.json";
        	String outputFileNameForCityNames = "/city.list_only_names.json";
        	String outputFileNameForCityIDs = "/city.list_only_ids.json";

        	
            Object obj = parser.parse(new FileReader(path + inputFileName ));
            FileWriter outputFileForCityNames = new FileWriter(path + outputFileNameForCityNames) ;
            FileWriter outputFileForCityIDs = new FileWriter(path + outputFileNameForCityIDs) ;

            JSONArray jsonArray = (JSONArray) obj;
            
            for(int i=0;i<=numberOfCities;i++) {
                Object obj2 = jsonArray.get(i);
                JSONObject jsonObject2 = (JSONObject) obj2;
                outputFileForCityNames.write(jsonObject2.get("name").toString());
                outputFileForCityIDs.write(jsonObject2.get("id").toString());
                outputFileForCityNames.write("\n");
                outputFileForCityIDs.write("\n");

                System.out.println(jsonObject2.get("name"));
                System.out.println(jsonObject2.get("id"));


            }
            outputFileForCityNames.flush();;
            outputFileForCityNames.close();
            
            outputFileForCityIDs.flush();;
            outputFileForCityIDs.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

	}
}
